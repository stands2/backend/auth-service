"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplateService = void 0;
const moleculer_1 = require("moleculer");
class TemplateService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.broker = broker;
        this.ActionHello = () => __awaiter(this, void 0, void 0, function* () {
            return 'Hello Moleculer';
        });
        this.ActionWelcome = (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.emit('name.welcome', ctx.params.name);
            return this.sayWelcome(ctx.params.name);
        });
        this.sayWelcome = (name) => {
            this.logger.info('Say hello to', name);
            return `Welcome, ${name}`;
        };
        this.userCreated = (ctx) => __awaiter(this, void 0, void 0, function* () {
            this.sum(ctx.params.a, ctx.params.b);
        });
        this.sumMethod = (a, b) => {
            return a + b;
        };
        this.serviceCreated = () => {
            this.logger.info('ES6 Service created');
        };
        this.serviceStarted = () => {
            this.logger.info('ES6 Service started.');
        };
        this.serviceStopped = () => {
            this.logger.info('ES6 Service stopped.');
        };
        this.parseServiceSchema({
            name: 'template',
            settings: {},
            actions: {
                hello: {
                    handler: this.ActionHello,
                },
                welcome: {
                    cache: {
                        keys: ['name'],
                    },
                    params: {
                        name: 'string',
                    },
                    handler: this.ActionWelcome,
                },
            },
            events: {
                'user.created': this.userCreated,
            },
            methods: {
                sum: this.sumMethod,
            },
            created: this.serviceCreated,
            started: this.serviceStarted,
            stopped: this.serviceStopped,
        });
    }
}
exports.TemplateService = TemplateService;
exports.default = TemplateService;
//# sourceMappingURL=template.service.js.map