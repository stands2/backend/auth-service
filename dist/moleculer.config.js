"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ecsFormat = require('@elastic/ecs-pino-format');
const brokerConfig = {
    namespace: 'stands',
    metadata: {},
    logger: {
        type: 'Pino',
        options: {
            pino: {
                options: ecsFormat(),
            },
        },
    },
    logLevel: 'info',
    cacher: {
        type: 'Memory',
        options: {},
    },
    serializer: 'ProtoBuf',
    requestTimeout: 10 * 1000,
    retryPolicy: {
        enabled: false,
        retries: 5,
        delay: 100,
        maxDelay: 1000,
        factor: 2,
    },
    maxCallLevel: 100,
    heartbeatInterval: 10,
    heartbeatTimeout: 30,
    contextParamsCloning: false,
    tracking: {
        enabled: false,
        shutdownTimeout: 5000,
    },
    disableBalancer: false,
    registry: {
        discoverer: 'etcd3://etcd:2379',
        strategy: 'RoundRobin',
        preferLocal: true,
    },
    circuitBreaker: {
        enabled: false,
        threshold: 0.5,
        minRequestCount: 20,
        windowTime: 60,
        halfOpenTime: 10 * 1000,
    },
    bulkhead: {
        enabled: false,
        concurrency: 10,
        maxQueueSize: 100,
    },
    validator: true,
    metrics: {
        enabled: true,
        reporter: {
            type: 'Console',
            options: {
                port: 3030,
                path: '/metrics',
                defaultLabels: (registry) => ({
                    namespace: registry.broker.namespace,
                    nodeID: registry.broker.nodeID,
                }),
            },
        },
    },
    tracing: {
        enabled: true,
        events: true,
        stackTrace: true,
        exporter: {
            type: 'Console',
            options: {
                logger: null,
                colors: true,
                width: 100,
                gaugeWidth: 40,
            },
        },
    },
    middlewares: [],
};
exports.default = brokerConfig;
//# sourceMappingURL=moleculer.config.js.map