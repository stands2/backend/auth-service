# @stands2/service-template

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/) [![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release&style=flat-square)](https://github.com/semantic-release/semantic-release)

[![Latest Release](https://gitlab.com/stands2/templates/service-template/-/badges/release.svg?style=flat-square)](https://gitlab.com/stands2/templates/service-template/-/releases) [![coverage report](https://gitlab.com/stands2/templates/service-template/badges/main/coverage.svg?style=flat-square)](https://gitlab.com/stands2/templates/service-template/-/commits/main)
[![pipeline status](https://gitlab.com/stands2/templates/service-template/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true)](https://gitlab.com/stands2/templates/service-template/-/commits/main)

## Build Setup

```bash
# Install dependencies
npm install

# Start developing with REPL
npm run dev

# Start production
npm start

# Run unit tests
npm test

# Run continuous test mode
npm run ci

# Run TSLint
npm run lint
```

## Run in Docker

**Build Docker image**

```bash
$ docker build -t test-nano .
```

**Start container**

```bash
$ docker run -d test-nano
```
